<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Board\BoardController;
use App\Http\Controllers\Board\Column\BoardColumnController;
use App\Http\Controllers\Board\Column\Card\ColumnCardController;
use App\Http\Controllers\ListCardController;
use App\Http\Controllers\ListCardsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [LoginController::class, 'login'])->name('login');

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware(['auth:sanctum'])->group(function () {

    Route::get('list-cards', ListCardController::class)->name('list.cards');

    Route::post('column/{column}/card/order', [ColumnCardController::class, 'order'])->name('card.order');
    Route::post('board/{board}/column/order', [BoardColumnController::class, 'order'])->name('board.order');

    Route::resource('column.card', ColumnCardController::class);
    Route::resource('board.column', BoardColumnController::class);
    Route::resource('board', BoardController::class);
});
