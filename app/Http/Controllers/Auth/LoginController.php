<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request) {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:8'
        ]);

        $user = User::where('email', $request->email)->first();

        if(!$user) {
            return response()->json([
                'errors' => [
                    'email' => ['User does not exist in our records.']
                ],
                'message' => "The given data was invalid."
            ], 404);
        }

        // checking password
        if (Hash::check($request->password, $user->password)) {
            $token = $user->createToken("api_login");
            return response()->json([
                'user' => $user,
                'token' => $token->plainTextToken
            ]);
        }

        return response()->json([
            'errors' => [
                'password' => ['Password Missmatch.']
            ],
            'message' => "The given data was invalid."
        ], 404);
    }
}
