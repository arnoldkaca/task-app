<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DatabaseDumper extends Controller {

    public function __invoke() {
        \Spatie\DbDumper\Databases\MySql::create()
            ->setDbName(env('DB_DATABASE'))
            ->setUserName(env('DB_USERNAME'))
            ->setPassword(env('DB_PASSWORD'))
            ->dumpToFile('dump.sql');


        $headers = array(
            'Content-Type: text/sql',
        );
        return response()->download(public_path('dump.sql'), 'dump.sql', $headers);

    }

}
