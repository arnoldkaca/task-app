<?php

namespace App\Http\Controllers\Board;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Board;

class BoardController extends Controller {

    public function __construct() {

    }
    public function index(Request $request) {
        $board = Board::all();
        return response()->json($board);
    }
    public function store(Request $request) {

    }

}
