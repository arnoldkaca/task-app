<?php

namespace App\Http\Controllers\Board\Column\Card;

use App\Http\Controllers\Controller;
use App\Models\Card;
use App\Models\Column;
use Illuminate\Http\Request;

class ColumnCardController extends Controller {

    public function __construct() {

    }

    public function index(Request $request, Column $column) {
        $cards = $column->cards()->orderBy('order_index', 'ASC')->get();
        return response()->json($cards);
    }

    public function store(Request $request, Column $column) {
        $card = Card::create([
            'title' => $request->title,
            'description' => $request->description,
            'column_id' => $column->id
        ]);
        return response()->json($card, 201);
    }

    public function update(Request $request, Column $column, Card $card) {
        Card::where('id', $card->id)->update([
            'title' => $request->title,
            'description' => $request->description,
        ]);
        $card->title = $request->title;
        $card->description = $request->description;
        $card->save;
        return response()->json($card, 201);
    }

    public function destroy(Request $request, Column $column, Card $card) {
        $card->delete();
        return response()->json([], 200);
    }

    public function order(Request $request, Column $column) {
        Card::where('id', $request->card)->update([
            'column_id' => $request->to
        ]);
        $cards = $column->cards()->orderBy('order_index', 'ASC')->get();
        $card_items = collect([]);
        $card_item = null;
        foreach ($cards as $key => $card)
            if($card->id != $request->card) $card_items->push($card);
            else $card_item = $card;

        $ordered_cards = collect();
        $ordered_index = 0;
        for ($i=0; $i < $card_items->count()+1; $i++) {
            if($i == $request->index) {
                $card_item['order_index'] = $ordered_index;
                $ordered_cards->push($card_item);
                $ordered_index++;
            }
            if(isset($card_items[$i])) {
                $card_items[$i]['order_index'] = $ordered_index;
                $ordered_index++;
                $ordered_cards->push($card_items[$i]);
            }
        }

        $cards = $ordered_cards->map(function($card, $index){
            return [
                'id' => $card['id'],
                'order_index' => $card['order_index'],
                'title' => $card['title'],
                'description' => $card['description'],
                'column_id' => $card['column_id']
            ];
        });
        Card::upsert($cards->toArray(), ['id'], ['order_index']);
        return response()->json([], 200);
    }


}
