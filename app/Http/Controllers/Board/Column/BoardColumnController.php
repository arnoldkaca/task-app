<?php

namespace App\Http\Controllers\Board\Column;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Board;
use App\Models\Column;

class BoardColumnController extends Controller {

    public function __construct() {

    }

    public function index(Request $request, Board $board) {
        $columns = $board->columns()->orderBy('order_index', 'ASC')->get();
        return response()->json($columns);
    }

    public function store(Request $request, Board $board) {
        $column = Column::create([
            'title' => $request->title,
            'board_id' => $board->id
        ]);
        return response()->json($column, 201);
    }

    public function destroy(Request $request, Board $board, Column $column) {
        $column->delete();
        return response()->json([], 200);
    }

    public function order(Request $request, Board $board) {
        $columns = collect($request->columns);
        $columns = $columns->map(function($column, $index){
            return [
                'id' => $column['id'],
                'order_index' => $index,
                'title' => $column['title']
            ];
        });
        Column::upsert($columns->toArray(), ['id'], ['order_index']);
        return response()->json([], 200);
    }
}
