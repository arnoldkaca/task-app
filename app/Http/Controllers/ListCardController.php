<?php

namespace App\Http\Controllers;

use App\Models\Card;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ListCardController extends Controller {
    public function __invoke(Request $request) {

        $cards = new Card;

        if($request->has('date')) {
            $from = null;
            $to = null;
            $from = Carbon::createFromFormat('Y-m-d', $request->date);
            $to = Carbon::createFromFormat('Y-m-d', $request->date);
            $from = $from->startOfDay();
            $to = $to->addDays(1)->startOfDay();
            $cards = $cards->whereBetween('updated_at', [$from->format('Y-m-d H:i:s'), $to->format('Y-m-d H:i:s')]);
        }

        if($request->has('status') && $request->status == 0) {
            $cards = $cards->withTrashed()->whereNotNull('deleted_at');
        } else if($request->has('status') && $request->status == 1) {
            $cards = $cards->whereNull('deleted_at');
        } else if(!$request->has('status')) {
            $cards = $cards->withTrashed();
        }

        $cards = $cards->get();
        return response()->json($cards);
    }
}
