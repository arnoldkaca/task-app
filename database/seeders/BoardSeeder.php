<?php

namespace Database\Seeders;

use App\Models\Board;
use Illuminate\Database\Seeder;

class BoardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $board = Board::first();
        if(!$board) {
            Board::create([
                'title' => 'Sample Static Board'
            ]);
        }
    }
}
