/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


require('./bootstrap');
window.Vue = require('vue').default;
// window.VueRouter = require('vue-router').default;

window.checkLogin = async function() {
    let user = localStorage.getItem('user');
    if(!user) {
        window.location.assign('/')
        return false
    }

    await axios.get('/api/user').then(res => {}).catch(err => {
        localStorage.removeItem('user')
        window.location.assign('/login')
    })
}

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('board', require('./components/Board').default);
Vue.component('navbar-component', require('./components/Navbar').default);
Vue.component("modal", { template: "#modal-template" });
const Home = Vue.component('home-page', require('./Home').default)
const Login = Vue.component('login-page', require('./auth/Login').default);
const Register = Vue.component('register-page', require('./auth/Register').default);

// const routes = [
//     { path: '/home', component: Home },
//     { path: '/login', component: Login },
//     { path: '/register', component: Register }
// ]

// const router = new VueRouter({ routes, mode: 'history' })
// const app = new Vue({ router }).$mount('#app')
const app = new Vue({
    el: '#app'
});


